// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatRoleCase extends HandleBaseCase {
  HandleQChatRoleCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    if (className?.trim() != 'QChatRoleService') {
      return null;
    }
    var map = params![0];
    switch (methodName) {
      case 'createServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .createServerRole(QChatCreateServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'deleteServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .deleteServerRole(QChatDeleteServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .updateServerRole(QChatUpdateServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateServerRolePriorities':
        final ret = await NimCore.instance.qChatRoleService
            .updateServerRolePriorities(
                QChatUpdateServerRolePrioritiesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServerRoles':
        final ret = await NimCore.instance.qChatRoleService
            .getServerRoles(QChatGetServerRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'addChannelRole':
        final ret = await NimCore.instance.qChatRoleService
            .addChannelRole(QChatAddChannelRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'removeChannelRole':
        final ret = await NimCore.instance.qChatRoleService
            .removeChannelRole(QChatRemoveChannelRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateChannelRole':
        final ret = await NimCore.instance.qChatRoleService
            .updateChannelRole(QChatUpdateChannelRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelRoles':
        final ret = await NimCore.instance.qChatRoleService
            .getChannelRoles(QChatGetChannelRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'addMembersToServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .addMembersToServerRole(
                QChatAddMembersToServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'removeMembersFromServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .removeMembersFromServerRole(
                QChatRemoveMembersFromServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMembersFromServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .getMembersFromServerRole(
                QChatGetMembersFromServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServerRolesByAccid':
        final ret = await NimCore.instance.qChatRoleService
            .getServerRolesByAccid(
                QChatGetServerRolesByAccidParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingServerRolesByAccids':
        final ret = await NimCore.instance.qChatRoleService
            .getExistingServerRolesByAccids(
                QChatGetExistingServerRolesByAccidsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingAccidsInServerRole':
        final ret = await NimCore.instance.qChatRoleService
            .getExistingAccidsInServerRole(
                QChatGetExistingAccidsInServerRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingChannelRolesByServerRoleIds':
        final ret = await NimCore.instance.qChatRoleService
            .getExistingChannelRolesByServerRoleIds(
                QChatGetExistingChannelRolesByServerRoleIdsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingAccidsOfMemberRoles':
        final ret = await NimCore.instance.qChatRoleService
            .getExistingAccidsOfMemberRoles(
                QChatGetExistingAccidsOfMemberRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMemberRoles':
        final ret = await NimCore.instance.qChatRoleService
            .getMemberRoles(QChatGetMemberRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'addMemberRole':
        final ret = await NimCore.instance.qChatRoleService
            .addMemberRole(QChatAddMemberRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'removeMemberRole':
        final ret = await NimCore.instance.qChatRoleService
            .removeMemberRole(QChatRemoveMemberRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateMemberRole':
        final ret = await NimCore.instance.qChatRoleService
            .updateMemberRole(QChatUpdateMemberRoleParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'checkPermission':
        final ret = await NimCore.instance.qChatRoleService
            .checkPermission(QChatCheckPermissionParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      // case 'checkPermissions':
      //   final ret = await NimCore.instance.qChatRoleService
      //       .checkPermissions(QChatCheckPermissionsParam.fromJson(map));
      //   result = ret;
      //   handled = true;
      //   break;
    }
    if (handled) {
      print('qChatRoleService#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
