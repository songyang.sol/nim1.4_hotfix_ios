// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatObserverCase extends HandleBaseCase {
  HandleQChatObserverCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    if (className?.trim() != 'QChatObserve') {
      return null;
    }
    switch (methodName) {
      case "onStatusChange":
        isCallback = true;
        NimCore.instance.qChatObserver.onStatusChange.listen((event) {
          print('QChatObserver#onStatusChange ${event.toJson()}');
          IntegratedManager.instance.report(
              'onStatusChange', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onMultiSpotLogin":
        isCallback = true;
        NimCore.instance.qChatObserver.onMultiSpotLogin.listen((event) {
          print('QChatObserver#onMultiSpotLogin ${event.toJson()}');
          IntegratedManager.instance.report(
              'onMultiSpotLogin', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onKickedOut":
        isCallback = true;
        NimCore.instance.qChatObserver.onKickedOut.listen((event) {
          print('QChatObserver#onKickedOut ${event.toJson()}');
          IntegratedManager.instance
              .report('onKickedOut', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onReceiveMessage":
        isCallback = true;
        NimCore.instance.qChatObserver.onReceiveMessage.listen((event) {
          print(
              'QChatObserver#onReceiveMessage ${event.map((e) => e.toJson()).toList()}');
          IntegratedManager.instance.report('onReceiveMessage',
              ResultBean(code: 0, data: event.map((e) => e.toJson()).toList()));
        });
        break;
      case "onMessageUpdate":
        isCallback = true;
        NimCore.instance.qChatObserver.onMessageUpdate.listen((event) {
          print('QChatObserver#onMessageUpdate ${event.toJson()}');
          IntegratedManager.instance.report(
              'onMessageUpdate', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onMessageRevoke":
        isCallback = true;
        NimCore.instance.qChatObserver.onMessageRevoke.listen((event) {
          print('QChatObserver#onMessageRevoke ${event.toJson()}');
          IntegratedManager.instance.report(
              'onMessageRevoke', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onMessageDelete":
        isCallback = true;
        NimCore.instance.qChatObserver.onMessageDelete.listen((event) {
          print('QChatObserver#onMessageDelete ${event.toJson()}');
          IntegratedManager.instance.report(
              'onMessageDelete', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onUnreadInfoChanged":
        isCallback = true;
        NimCore.instance.qChatObserver.onUnreadInfoChanged.listen((event) {
          print('QChatObserver#onUnreadInfoChanged ${event.toJson()}');
          IntegratedManager.instance.report(
              'onUnreadInfoChanged', ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onMessageStatusChange":
        isCallback = true;
        NimCore.instance.qChatObserver.onMessageStatusChange.listen((event) {
          print('QChatObserver#onMessageStatusChange ${event.toJson()}');
          IntegratedManager.instance.report('onMessageStatusChange',
              ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onAttachmentProgress":
        isCallback = true;
        NimCore.instance.qChatObserver.onAttachmentProgress.listen((event) {
          print('QChatObserver#onAttachmentProgress ${event.toJson()}');
          IntegratedManager.instance.report('onAttachmentProgress',
              ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "onReceiveSystemNotification":
        isCallback = true;
        NimCore.instance.qChatObserver.onReceiveSystemNotification
            .listen((event) {
          print(
              'QChatObserver#onReceiveSystemNotification ${event.map((e) => e.toJson()).toList()}');
          IntegratedManager.instance.report('onReceiveSystemNotification',
              ResultBean(code: 0, data: event.map((e) => e.toJson()).toList()));
        });
        break;
      case "onSystemNotificationUpdate":
        isCallback = true;
        NimCore.instance.qChatObserver.onSystemNotificationUpdate
            .listen((event) {
          print('QChatObserver#onSystemNotificationUpdate ${event.toJson()}');
          IntegratedManager.instance.report('onSystemNotificationUpdate',
              ResultBean(code: 0, data: event.toJson()));
        });
        break;
      case "serverUnreadInfoChanged":
        isCallback = true;
        NimCore.instance.qChatObserver.serverUnreadInfoChanged.listen((event) {
          print('QChatObserver#serverUnreadInfoChanged ${event.toJson()}');
          IntegratedManager.instance.report('serverUnreadInfoChanged',
              ResultBean(code: 0, data: event.toJson()));
        });
        break;
    }
    if (handled) {
      print('QChatObserver#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
