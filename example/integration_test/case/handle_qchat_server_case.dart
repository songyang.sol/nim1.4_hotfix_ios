// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatServerCase extends HandleBaseCase {
  HandleQChatServerCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    className = className?.trim();
    if (className != 'QChatServerService') {
      return null;
    }
    var map = params![0];
    switch (methodName) {
      case 'acceptServerApply':
        final ret = await NimCore.instance.qChatServerService
            .acceptServerApply(QChatAcceptServerApplyParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'acceptServerInvite':
        final ret = await NimCore.instance.qChatServerService
            .acceptServerInvite(QChatAcceptServerInviteParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'applyServerJoin':
        final ret = await NimCore.instance.qChatServerService
            .applyServerJoin(QChatApplyServerJoinParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'createServer':
        final ret = await NimCore.instance.qChatServerService
            .createServer(QChatCreateServerParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'deleteServer':
        final ret = await NimCore.instance.qChatServerService
            .deleteServer(QChatDeleteServerParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServerMembers':
        final ret = await NimCore.instance.qChatServerService
            .getServerMembers(QChatGetServerMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServerMembersByPage':
        final ret = await NimCore.instance.qChatServerService
            .getServerMembersByPage(
                QChatGetServerMembersByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServers':
        final ret = await NimCore.instance.qChatServerService
            .getServers(QChatGetServersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getServersByPage':
        final ret = await NimCore.instance.qChatServerService
            .getServersByPage(QChatGetServersByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'inviteServerMembers':
        final ret = await NimCore.instance.qChatServerService
            .inviteServerMembers(QChatInviteServerMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'kickServerMembers':
        final ret = await NimCore.instance.qChatServerService
            .kickServerMembers(QChatKickServerMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'leaveServer':
        final ret = await NimCore.instance.qChatServerService
            .leaveServer(QChatLeaveServerParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'rejectServerApply':
        final ret = await NimCore.instance.qChatServerService
            .rejectServerApply(QChatRejectServerApplyParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'rejectServerInvite':
        final ret = await NimCore.instance.qChatServerService
            .rejectServerInvite(QChatRejectServerInviteParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateServer':
        final ret = await NimCore.instance.qChatServerService
            .updateServer(QChatUpdateServerParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateMyMemberInfo':
        final ret = await NimCore.instance.qChatServerService
            .updateMyMemberInfo(QChatUpdateMyMemberInfoParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'subscribeServer':
        final ret = await NimCore.instance.qChatServerService
            .subscribeServer(QChatSubscribeServerParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'searchServerByPage':
        final ret = await NimCore.instance.qChatServerService
            .searchServerByPage(QChatSearchServerByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'generateInviteCode':
        final ret = await NimCore.instance.qChatServerService
            .generateInviteCode(QChatGenerateInviteCodeParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'joinByInviteCode':
        final ret = await NimCore.instance.qChatServerService
            .joinByInviteCode(QChatJoinByInviteCodeParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateServerMemberInfo':
        final ret = await NimCore.instance.qChatServerService
            .updateServerMemberInfo(
                QChatUpdateServerMemberInfoParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'banServerMember':
        final ret = await NimCore.instance.qChatServerService
            .banServerMember(QChatBanServerMemberParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'unbanServerMember':
        final ret = await NimCore.instance.qChatServerService
            .unbanServerMember(QChatUnbanServerMemberParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getBannedServerMembersByPage':
        final ret = await NimCore.instance.qChatServerService
            .getBannedServerMembersByPage(
                QChatGetBannedServerMembersByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateUserServerPushConfig':
        final ret = await NimCore.instance.qChatServerService
            .updateUserServerPushConfig(
                QChatUpdateUserServerPushConfigParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getUserServerPushConfigs':
        final ret = await NimCore.instance.qChatServerService
            .getUserServerPushConfigs(
                QChatGetUserServerPushConfigsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'searchServerMemberByPage':
        final ret = await NimCore.instance.qChatServerService
            .searchServerMemberByPage(
                QChatSearchServerMemberByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'markRead':
        final ret = await NimCore.instance.qChatServerService
            .markRead(QChatServerMarkReadParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'subscribeAllChannel':
        final ret = await NimCore.instance.qChatServerService
            .subscribeAllChannel(QChatSubscribeAllChannelParam.fromJson(map));
        result = ret;
        handled = true;
        break;
    }
    if (handled) {
      print('qChatServerService#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
