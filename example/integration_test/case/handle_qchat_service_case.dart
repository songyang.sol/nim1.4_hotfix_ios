// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatServiceCase extends HandleBaseCase {
  HandleQChatServiceCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    if (className != 'QChatService') {
      return null;
    }
    var map;
    if (params!.isNotEmpty) {
      map = params![0];
    }

    switch (methodName) {
      case 'login':
        final ret = await NimCore.instance.qChatService
            .login(QChatLoginParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'logout':
        final ret = await NimCore.instance.qChatService.logout();
        result = ret;
        handled = true;
        break;
      case 'kickOtherClients':
        final ret = await NimCore.instance.qChatService
            .kickOtherClients(QChatKickOtherClientsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
    }
    if (handled) {
      print('qChatService#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
