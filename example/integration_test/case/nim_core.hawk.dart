// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:nim_core/nim_core.dart';

class HawkQChatChannelService {
  static Future<dynamic> handleHawkMethodCall(
      Object? obj,
      String method,
      List<Map> params,
      Map<Type, Future<dynamic> Function(Map)> factory) async {
    final target = obj as QChatChannelService?;
    switch (method) {

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "createChannel",
      ///    "params": [
      ///      {
      ///        "param": "QChatCreateChannelParam"
      ///      }
      ///    ]
      ///  }
      case 'createChannel':
        {
          var argParam;
          argParam =
              ((await factory[QChatCreateChannelParam]?.call(params[0])) ??
                      QChatCreateChannelParam.fromJson((params[0]).cast()))
                  as QChatCreateChannelParam;
          return [
            target!.createChannel(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "deleteChannel",
      ///    "params": [
      ///      {
      ///        "param": "QChatDeleteChannelParam"
      ///      }
      ///    ]
      ///  }
      case 'deleteChannel':
        {
          var argParam;
          argParam =
              ((await factory[QChatDeleteChannelParam]?.call(params[0])) ??
                      QChatDeleteChannelParam.fromJson((params[0]).cast()))
                  as QChatDeleteChannelParam;
          return [
            target!.deleteChannel(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "updateChannel",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateChannelParam"
      ///      }
      ///    ]
      ///  }
      case 'updateChannel':
        {
          var argParam;
          argParam =
              ((await factory[QChatUpdateChannelParam]?.call(params[0])) ??
                      QChatUpdateChannelParam.fromJson((params[0]).cast()))
                  as QChatUpdateChannelParam;
          return [
            target!.updateChannel(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannels",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelsParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannels':
        {
          var argParam;
          argParam = ((await factory[QChatGetChannelsParam]?.call(params[0])) ??
                  QChatGetChannelsParam.fromJson((params[0]).cast()))
              as QChatGetChannelsParam;
          return [
            target!.getChannels(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannelsByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelsByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannelsByPage':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetChannelsByPageParam]?.call(params[0])) ??
                      QChatGetChannelsByPageParam.fromJson((params[0]).cast()))
                  as QChatGetChannelsByPageParam;
          return [
            target!.getChannelsByPage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannelMembersByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelMembersByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannelMembersByPage':
        {
          var argParam;
          argParam = ((await factory[QChatGetChannelMembersByPageParam]
                  ?.call(params[0])) ??
              QChatGetChannelMembersByPageParam.fromJson(
                  (params[0]).cast())) as QChatGetChannelMembersByPageParam;
          return [
            target!.getChannelMembersByPage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannelUnreadInfos",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelUnreadInfosParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannelUnreadInfos':
        {
          var argParam;
          argParam = ((await factory[QChatGetChannelUnreadInfosParam]
                      ?.call(params[0])) ??
                  QChatGetChannelUnreadInfosParam.fromJson((params[0]).cast()))
              as QChatGetChannelUnreadInfosParam;
          return [
            target!.getChannelUnreadInfos(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "subscribeChannel",
      ///    "params": [
      ///      {
      ///        "param": "QChatSubscribeChannelParam"
      ///      }
      ///    ]
      ///  }
      case 'subscribeChannel':
        {
          var argParam;
          argParam =
              ((await factory[QChatSubscribeChannelParam]?.call(params[0])) ??
                      QChatSubscribeChannelParam.fromJson((params[0]).cast()))
                  as QChatSubscribeChannelParam;
          return [
            target!.subscribeChannel(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "searchChannelByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatSearchChannelByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'searchChannelByPage':
        {
          var argParam;
          argParam = ((await factory[QChatSearchChannelByPageParam]
                      ?.call(params[0])) ??
                  QChatSearchChannelByPageParam.fromJson((params[0]).cast()))
              as QChatSearchChannelByPageParam;
          return [
            target!.searchChannelByPage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "searchChannelMembers",
      ///    "params": [
      ///      {
      ///        "param": "QChatSearchChannelMembersParam"
      ///      }
      ///    ]
      ///  }
      case 'searchChannelMembers':
        {
          var argParam;
          argParam = ((await factory[QChatSearchChannelMembersParam]
                      ?.call(params[0])) ??
                  QChatSearchChannelMembersParam.fromJson((params[0]).cast()))
              as QChatSearchChannelMembersParam;
          return [
            target!.searchChannelMembers(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "updateChannelBlackWhiteRoles",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateChannelBlackWhiteRolesParam"
      ///      }
      ///    ]
      ///  }
      case 'updateChannelBlackWhiteRoles':
        {
          var argParam;
          argParam = ((await factory[QChatUpdateChannelBlackWhiteRolesParam]
                      ?.call(params[0])) ??
                  QChatUpdateChannelBlackWhiteRolesParam.fromJson(
                      (params[0]).cast()))
              as QChatUpdateChannelBlackWhiteRolesParam;
          return [
            target!.updateChannelBlackWhiteRoles(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannelBlackWhiteRolesByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelBlackWhiteRolesByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannelBlackWhiteRolesByPage':
        {
          var argParam;
          argParam = ((await factory[QChatGetChannelBlackWhiteRolesByPageParam]
                      ?.call(params[0])) ??
                  QChatGetChannelBlackWhiteRolesByPageParam.fromJson(
                      (params[0]).cast()))
              as QChatGetChannelBlackWhiteRolesByPageParam;
          return [
            target!.getChannelBlackWhiteRolesByPage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getExistingChannelBlackWhiteRoles",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetExistingChannelBlackWhiteRolesParam"
      ///      }
      ///    ]
      ///  }
      case 'getExistingChannelBlackWhiteRoles':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetExistingChannelBlackWhiteRolesParam]
                          ?.call(params[0])) ??
                      QChatGetExistingChannelBlackWhiteRolesParam.fromJson(
                          (params[0]).cast()))
                  as QChatGetExistingChannelBlackWhiteRolesParam;
          return [
            target!.getExistingChannelBlackWhiteRoles(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "updateChannelBlackWhiteMembers",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateChannelBlackWhiteMembersParam"
      ///      }
      ///    ]
      ///  }
      case 'updateChannelBlackWhiteMembers':
        {
          var argParam;
          argParam = ((await factory[QChatUpdateChannelBlackWhiteMembersParam]
                      ?.call(params[0])) ??
                  QChatUpdateChannelBlackWhiteMembersParam.fromJson(
                      (params[0]).cast()))
              as QChatUpdateChannelBlackWhiteMembersParam;
          return [
            target!.updateChannelBlackWhiteMembers(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getChannelBlackWhiteMembersByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetChannelBlackWhiteMembersByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'getChannelBlackWhiteMembersByPage':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetChannelBlackWhiteMembersByPageParam]
                          ?.call(params[0])) ??
                      QChatGetChannelBlackWhiteMembersByPageParam.fromJson(
                          (params[0]).cast()))
                  as QChatGetChannelBlackWhiteMembersByPageParam;
          return [
            target!.getChannelBlackWhiteMembersByPage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getExistingChannelBlackWhiteMembers",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetExistingChannelBlackWhiteMembersParam"
      ///      }
      ///    ]
      ///  }
      case 'getExistingChannelBlackWhiteMembers':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetExistingChannelBlackWhiteMembersParam]
                          ?.call(params[0])) ??
                      QChatGetExistingChannelBlackWhiteMembersParam.fromJson(
                          (params[0]).cast()))
                  as QChatGetExistingChannelBlackWhiteMembersParam;
          return [
            target!.getExistingChannelBlackWhiteMembers(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "updateUserChannelPushConfig",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateUserChannelPushConfigParam"
      ///      }
      ///    ]
      ///  }
      case 'updateUserChannelPushConfig':
        {
          var argParam;
          argParam = ((await factory[QChatUpdateUserChannelPushConfigParam]
                  ?.call(params[0])) ??
              QChatUpdateUserChannelPushConfigParam.fromJson(
                  (params[0]).cast())) as QChatUpdateUserChannelPushConfigParam;
          return [
            target!.updateUserChannelPushConfig(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatChannelService",
      ///    "methodName": "getUserChannelPushConfigs",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetUserChannelPushConfigsParam"
      ///      }
      ///    ]
      ///  }
      case 'getUserChannelPushConfigs':
        {
          var argParam;
          argParam = ((await factory[QChatGetUserChannelPushConfigsParam]
                  ?.call(params[0])) ??
              QChatGetUserChannelPushConfigsParam.fromJson(
                  (params[0]).cast())) as QChatGetUserChannelPushConfigsParam;
          return [
            target!.getUserChannelPushConfigs(
              argParam,
            )
          ];
        }
    }
    return null;
  }
}

class HawkQChatMessageService {
  static Future<dynamic> handleHawkMethodCall(
      Object? obj,
      String method,
      List<Map> params,
      Map<Type, Future<dynamic> Function(Map)> factory) async {
    final target = obj as QChatMessageService?;
    switch (method) {

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "sendMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatSendMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'sendMessage':
        {
          var argParam;
          argParam = ((await factory[QChatSendMessageParam]?.call(params[0])) ??
                  QChatSendMessageParam.fromJson((params[0]).cast()))
              as QChatSendMessageParam;
          return [
            target!.sendMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "resendMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatResendMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'resendMessage':
        {
          var argParam;
          argParam =
              ((await factory[QChatResendMessageParam]?.call(params[0])) ??
                      QChatResendMessageParam.fromJson((params[0]).cast()))
                  as QChatResendMessageParam;
          return [
            target!.resendMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "downloadAttachment",
      ///    "params": [
      ///      {
      ///        "param": "QChatDownloadAttachmentParam"
      ///      }
      ///    ]
      ///  }
      case 'downloadAttachment':
        {
          var argParam;
          argParam =
              ((await factory[QChatDownloadAttachmentParam]?.call(params[0])) ??
                      QChatDownloadAttachmentParam.fromJson((params[0]).cast()))
                  as QChatDownloadAttachmentParam;
          return [
            target!.downloadAttachment(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getMessageHistory",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetMessageHistoryParam"
      ///      }
      ///    ]
      ///  }
      case 'getMessageHistory':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetMessageHistoryParam]?.call(params[0])) ??
                      QChatGetMessageHistoryParam.fromJson((params[0]).cast()))
                  as QChatGetMessageHistoryParam;
          return [
            target!.getMessageHistory(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "updateMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'updateMessage':
        {
          var argParam;
          argParam =
              ((await factory[QChatUpdateMessageParam]?.call(params[0])) ??
                      QChatUpdateMessageParam.fromJson((params[0]).cast()))
                  as QChatUpdateMessageParam;
          return [
            target!.updateMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "revokeMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatRevokeMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'revokeMessage':
        {
          var argParam;
          argParam =
              ((await factory[QChatRevokeMessageParam]?.call(params[0])) ??
                      QChatRevokeMessageParam.fromJson((params[0]).cast()))
                  as QChatRevokeMessageParam;
          return [
            target!.revokeMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "deleteMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatDeleteMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'deleteMessage':
        {
          var argParam;
          argParam =
              ((await factory[QChatDeleteMessageParam]?.call(params[0])) ??
                      QChatDeleteMessageParam.fromJson((params[0]).cast()))
                  as QChatDeleteMessageParam;
          return [
            target!.deleteMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "markMessageRead",
      ///    "params": [
      ///      {
      ///        "param": "QChatMarkMessageReadParam"
      ///      }
      ///    ]
      ///  }
      case 'markMessageRead':
        {
          var argParam;
          argParam =
              ((await factory[QChatMarkMessageReadParam]?.call(params[0])) ??
                      QChatMarkMessageReadParam.fromJson((params[0]).cast()))
                  as QChatMarkMessageReadParam;
          return [
            target!.markMessageRead(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "sendSystemNotification",
      ///    "params": [
      ///      {
      ///        "param": "QChatSendSystemNotificationParam"
      ///      }
      ///    ]
      ///  }
      case 'sendSystemNotification':
        {
          var argParam;
          argParam = ((await factory[QChatSendSystemNotificationParam]
                      ?.call(params[0])) ??
                  QChatSendSystemNotificationParam.fromJson((params[0]).cast()))
              as QChatSendSystemNotificationParam;
          return [
            target!.sendSystemNotification(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "resendSystemNotification",
      ///    "params": [
      ///      {
      ///        "param": "QChatResendSystemNotificationParam"
      ///      }
      ///    ]
      ///  }
      case 'resendSystemNotification':
        {
          var argParam;
          argParam = ((await factory[QChatResendSystemNotificationParam]
                  ?.call(params[0])) ??
              QChatResendSystemNotificationParam.fromJson(
                  (params[0]).cast())) as QChatResendSystemNotificationParam;
          return [
            target!.resendSystemNotification(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "updateSystemNotification",
      ///    "params": [
      ///      {
      ///        "param": "QChatUpdateSystemNotificationParam"
      ///      }
      ///    ]
      ///  }
      case 'updateSystemNotification':
        {
          var argParam;
          argParam = ((await factory[QChatUpdateSystemNotificationParam]
                  ?.call(params[0])) ??
              QChatUpdateSystemNotificationParam.fromJson(
                  (params[0]).cast())) as QChatUpdateSystemNotificationParam;
          return [
            target!.updateSystemNotification(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "markSystemNotificationsRead",
      ///    "params": [
      ///      {
      ///        "param": "QChatMarkSystemNotificationsReadParam"
      ///      }
      ///    ]
      ///  }
      case 'markSystemNotificationsRead':
        {
          var argParam;
          argParam = ((await factory[QChatMarkSystemNotificationsReadParam]
                  ?.call(params[0])) ??
              QChatMarkSystemNotificationsReadParam.fromJson(
                  (params[0]).cast())) as QChatMarkSystemNotificationsReadParam;
          return [
            target!.markSystemNotificationsRead(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getMessageHistoryByIds",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetMessageHistoryByIdsParam"
      ///      }
      ///    ]
      ///  }
      case 'getMessageHistoryByIds':
        {
          var argParam;
          argParam = ((await factory[QChatGetMessageHistoryByIdsParam]
                      ?.call(params[0])) ??
                  QChatGetMessageHistoryByIdsParam.fromJson((params[0]).cast()))
              as QChatGetMessageHistoryByIdsParam;
          return [
            target!.getMessageHistoryByIds(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "replyMessage",
      ///    "params": [
      ///      {
      ///        "param": "QChatReplyMessageParam"
      ///      }
      ///    ]
      ///  }
      case 'replyMessage':
        {
          var argParam;
          argParam =
              ((await factory[QChatReplyMessageParam]?.call(params[0])) ??
                      QChatReplyMessageParam.fromJson((params[0]).cast()))
                  as QChatReplyMessageParam;
          return [
            target!.replyMessage(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "clearMsgNotifyAndroid",
      ///    "params": []
      ///  }
      case 'clearMsgNotifyAndroid':
        {
          return [target!.clearMsgNotifyAndroid()];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getReferMessages",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetReferMessagesParam"
      ///      }
      ///    ]
      ///  }
      case 'getReferMessages':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetReferMessagesParam]?.call(params[0])) ??
                      QChatGetReferMessagesParam.fromJson((params[0]).cast()))
                  as QChatGetReferMessagesParam;
          return [
            target!.getReferMessages(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getThreadMessages",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetThreadMessagesParam"
      ///      }
      ///    ]
      ///  }
      case 'getThreadMessages':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetThreadMessagesParam]?.call(params[0])) ??
                      QChatGetThreadMessagesParam.fromJson((params[0]).cast()))
                  as QChatGetThreadMessagesParam;
          return [
            target!.getThreadMessages(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getMessageThreadInfos",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetMessageThreadInfosParam"
      ///      }
      ///    ]
      ///  }
      case 'getMessageThreadInfos':
        {
          var argParam;
          argParam = ((await factory[QChatGetMessageThreadInfosParam]
                      ?.call(params[0])) ??
                  QChatGetMessageThreadInfosParam.fromJson((params[0]).cast()))
              as QChatGetMessageThreadInfosParam;
          return [
            target!.getMessageThreadInfos(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "addQuickComment",
      ///    "params": [
      ///      {
      ///        "param": "QChatAddQuickCommentParam"
      ///      }
      ///    ]
      ///  }
      case 'addQuickComment':
        {
          var argParam;
          argParam =
              ((await factory[QChatAddQuickCommentParam]?.call(params[0])) ??
                      QChatAddQuickCommentParam.fromJson((params[0]).cast()))
                  as QChatAddQuickCommentParam;
          return [
            target!.addQuickComment(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "removeQuickComment",
      ///    "params": [
      ///      {
      ///        "param": "QChatRemoveQuickCommentParam"
      ///      }
      ///    ]
      ///  }
      case 'removeQuickComment':
        {
          var argParam;
          argParam =
              ((await factory[QChatRemoveQuickCommentParam]?.call(params[0])) ??
                      QChatRemoveQuickCommentParam.fromJson((params[0]).cast()))
                  as QChatRemoveQuickCommentParam;
          return [
            target!.removeQuickComment(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getQuickComments",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetQuickCommentsParam"
      ///      }
      ///    ]
      ///  }
      case 'getQuickComments':
        {
          var argParam;
          argParam =
              ((await factory[QChatGetQuickCommentsParam]?.call(params[0])) ??
                      QChatGetQuickCommentsParam.fromJson((params[0]).cast()))
                  as QChatGetQuickCommentsParam;
          return [
            target!.getQuickComments(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getMessageCache",
      ///    "params": [
      ///      {
      ///        "qchatServerId": "int"
      ///      },
      ///      {
      ///        "qchatChannelId": "int"
      ///      }
      ///    ]
      ///  }
      case 'getMessageCache':
        {
          var argQchatServerId;
          argQchatServerId = params[0]['qchatServerId'] as int;
          var argQchatChannelId;
          argQchatChannelId = params[1]['qchatChannelId'] as int;
          return [
            target!.getMessageCache(
              argQchatServerId,
              argQchatChannelId,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "clearMessageCache",
      ///    "params": []
      ///  }
      case 'clearMessageCache':
        {
          return [target!.clearMessageCache()];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "getLastMessageOfChannels",
      ///    "params": [
      ///      {
      ///        "param": "QChatGetLastMessageOfChannelsParam"
      ///      }
      ///    ]
      ///  }
      case 'getLastMessageOfChannels':
        {
          var argParam;
          argParam = ((await factory[QChatGetLastMessageOfChannelsParam]
                  ?.call(params[0])) ??
              QChatGetLastMessageOfChannelsParam.fromJson(
                  (params[0]).cast())) as QChatGetLastMessageOfChannelsParam;
          return [
            target!.getLastMessageOfChannels(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatMessageService",
      ///    "methodName": "searchMsgByPage",
      ///    "params": [
      ///      {
      ///        "param": "QChatSearchMsgByPageParam"
      ///      }
      ///    ]
      ///  }
      case 'searchMsgByPage':
        {
          var argParam;
          argParam =
              ((await factory[QChatSearchMsgByPageParam]?.call(params[0])) ??
                      QChatSearchMsgByPageParam.fromJson((params[0]).cast()))
                  as QChatSearchMsgByPageParam;
          return [
            target!.searchMsgByPage(
              argParam,
            )
          ];
        }
    }
    return null;
  }
}

class HawkQChatObserver {
  static Future<dynamic> handleHawkMethodCall(
      Object? obj,
      String method,
      List<Map> params,
      Map<Type, Future<dynamic> Function(Map)> factory) async {
    final target = obj as QChatObserver?;
    switch (method) {

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnStatusChange",
      ///    "params": []
      ///  }
      case 'getOnStatusChange':
        {
          return [target!.onStatusChange];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnMultiSpotLogin",
      ///    "params": []
      ///  }
      case 'getOnMultiSpotLogin':
        {
          return [target!.onMultiSpotLogin];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnKickedOut",
      ///    "params": []
      ///  }
      case 'getOnKickedOut':
        {
          return [target!.onKickedOut];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnReceiveMessage",
      ///    "params": []
      ///  }
      case 'getOnReceiveMessage':
        {
          return [target!.onReceiveMessage];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnMessageUpdate",
      ///    "params": []
      ///  }
      case 'getOnMessageUpdate':
        {
          return [target!.onMessageUpdate];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnMessageRevoke",
      ///    "params": []
      ///  }
      case 'getOnMessageRevoke':
        {
          return [target!.onMessageRevoke];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnMessageDelete",
      ///    "params": []
      ///  }
      case 'getOnMessageDelete':
        {
          return [target!.onMessageDelete];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnUnreadInfoChanged",
      ///    "params": []
      ///  }
      case 'getOnUnreadInfoChanged':
        {
          return [target!.onUnreadInfoChanged];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnMessageStatusChange",
      ///    "params": []
      ///  }
      case 'getOnMessageStatusChange':
        {
          return [target!.onMessageStatusChange];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnAttachmentProgress",
      ///    "params": []
      ///  }
      case 'getOnAttachmentProgress':
        {
          return [target!.onAttachmentProgress];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnReceiveSystemNotification",
      ///    "params": []
      ///  }
      case 'getOnReceiveSystemNotification':
        {
          return [target!.onReceiveSystemNotification];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getOnSystemNotificationUpdate",
      ///    "params": []
      ///  }
      case 'getOnSystemNotificationUpdate':
        {
          return [target!.onSystemNotificationUpdate];
        }

      ///  {
      ///    "className": "QChatObserver",
      ///    "methodName": "getServerUnreadInfoChanged",
      ///    "params": []
      ///  }
      case 'getServerUnreadInfoChanged':
        {
          return [target!.serverUnreadInfoChanged];
        }
    }
    return null;
  }
}

class HawkQChatPushService {
  static Future<dynamic> handleHawkMethodCall(
      Object? obj,
      String method,
      List<Map> params,
      Map<Type, Future<dynamic> Function(Map)> factory) async {
    final target = obj as QChatPushService?;
    switch (method) {

      ///  {
      ///    "className": "QChatPushService",
      ///    "methodName": "enableAndroid",
      ///    "params": [
      ///      {
      ///        "enable": "bool"
      ///      }
      ///    ]
      ///  }
      case 'enableAndroid':
        {
          var argEnable;
          argEnable = params[0]['enable'] as bool;
          return [
            target!.enableAndroid(
              argEnable,
            )
          ];
        }

      ///  {
      ///    "className": "QChatPushService",
      ///    "methodName": "isEnableAndroid",
      ///    "params": []
      ///  }
      case 'isEnableAndroid':
        {
          return [target!.isEnableAndroid()];
        }

      ///  {
      ///    "className": "QChatPushService",
      ///    "methodName": "setPushConfig",
      ///    "params": [
      ///      {
      ///        "param": "QChatPushConfig"
      ///      }
      ///    ]
      ///  }
      case 'setPushConfig':
        {
          var argParam;
          argParam = ((await factory[QChatPushConfig]?.call(params[0])) ??
              QChatPushConfig.fromJson((params[0]).cast())) as QChatPushConfig;
          return [
            target!.setPushConfig(
              argParam,
            )
          ];
        }

      ///  {
      ///    "className": "QChatPushService",
      ///    "methodName": "getPushConfig",
      ///    "params": []
      ///  }
      case 'getPushConfig':
        {
          return [target!.getPushConfig()];
        }

      ///  {
      ///    "className": "QChatPushService",
      ///    "methodName": "isPushConfigExistAndroid",
      ///    "params": []
      ///  }
      case 'isPushConfigExistAndroid':
        {
          return [target!.isPushConfigExistAndroid()];
        }
    }
    return null;
  }
}
