// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatChannelCase extends HandleBaseCase {
  HandleQChatChannelCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    if (className != 'QChatChannelService') {
      return null;
    }
    var map = params![0];
    switch (methodName) {
      case 'createChannel':
        final ret = await NimCore.instance.qChatChannelService
            .createChannel(QChatCreateChannelParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'deleteChannel':
        final ret = await NimCore.instance.qChatChannelService
            .deleteChannel(QChatDeleteChannelParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateChannel':
        final ret = await NimCore.instance.qChatChannelService
            .updateChannel(QChatUpdateChannelParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannels':
        final ret = await NimCore.instance.qChatChannelService
            .getChannels(QChatGetChannelsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelsByPage':
        final ret = await NimCore.instance.qChatChannelService
            .getChannelsByPage(QChatGetChannelsByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelMembersByPage':
        final ret = await NimCore.instance.qChatChannelService
            .getChannelMembersByPage(
                QChatGetChannelMembersByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelUnreadInfos':
        final ret = await NimCore.instance.qChatChannelService
            .getChannelUnreadInfos(
                QChatGetChannelUnreadInfosParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'subscribeChannel':
        final ret = await NimCore.instance.qChatChannelService
            .subscribeChannel(QChatSubscribeChannelParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'searchChannelByPage':
        final ret = await NimCore.instance.qChatChannelService
            .searchChannelByPage(QChatSearchChannelByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'searchChannelMembers':
        final ret = await NimCore.instance.qChatChannelService
            .searchChannelMembers(QChatSearchChannelMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateUserChannelPushConfig':
        final ret = await NimCore.instance.qChatChannelService
            .updateUserChannelPushConfig(
                QChatUpdateUserChannelPushConfigParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateChannelBlackWhiteRoles':
        final ret = await NimCore.instance.qChatChannelService
            .updateChannelBlackWhiteRoles(
                QChatUpdateChannelBlackWhiteRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelBlackWhiteRolesByPage':
        final ret = await NimCore.instance.qChatChannelService
            .getChannelBlackWhiteRolesByPage(
                QChatGetChannelBlackWhiteRolesByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingChannelBlackWhiteRoles':
        final ret = await NimCore.instance.qChatChannelService
            .getExistingChannelBlackWhiteRoles(
                QChatGetExistingChannelBlackWhiteRolesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateChannelBlackWhiteMembers':
        final ret = await NimCore.instance.qChatChannelService
            .updateChannelBlackWhiteMembers(
                QChatUpdateChannelBlackWhiteMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getChannelBlackWhiteMembersByPage':
        final ret = await NimCore.instance.qChatChannelService
            .getChannelBlackWhiteMembersByPage(
                QChatGetChannelBlackWhiteMembersByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getExistingChannelBlackWhiteMembers':
        final ret = await NimCore.instance.qChatChannelService
            .getExistingChannelBlackWhiteMembers(
                QChatGetExistingChannelBlackWhiteMembersParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getUserChannelPushConfigs':
        final ret = await NimCore.instance.qChatChannelService
            .getUserChannelPushConfigs(
                QChatGetUserChannelPushConfigsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
    }
    if (handled) {
      print('QChatChannelService#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
