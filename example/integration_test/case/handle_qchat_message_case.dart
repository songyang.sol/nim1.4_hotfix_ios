// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'dart:convert';
import 'dart:io';

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';
import 'package:path_provider/path_provider.dart';

class HandleQChatMessageCase extends HandleBaseCase {
  HandleQChatMessageCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    var handled = false;
    var isCallback = false;
    var result;
    if (className != 'QChatMessageService') {
      return null;
    }
    var map = params![0];
    switch (methodName) {
      case 'sendMessage':
        var param = QChatSendMessageParam.fromJson(map);
        var filePath = map["filePath"] as String?;
        Directory rootPath = await getApplicationDocumentsDirectory();
        if (param.type == NIMMessageType.video && param.attach == "videoTest") {
          if (Platform.isIOS) {
            filePath = '${rootPath.path}/$filePath';
          }
          var fileAttachment =
              NIMVideoAttachment(size: 100, displayName: "test", url: filePath);

          param.attach = jsonEncode(fileAttachment.toMap());
        }
        if (param.type == NIMMessageType.image && Platform.isIOS) {
          var imageAttachment =
              NIMImageAttachment(path: '${rootPath.path}/$filePath', size: 100);

          param.attach = jsonEncode(imageAttachment.toMap());
        }
        final ret =
            await NimCore.instance.qChatMessageService.sendMessage(param);
        result = ret;
        handled = true;
        break;
      case 'resendMessage':
        final ret = await NimCore.instance.qChatMessageService
            .resendMessage(QChatResendMessageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'downloadAttachment':
        final ret = await NimCore.instance.qChatMessageService
            .downloadAttachment(QChatDownloadAttachmentParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMessageHistory':
        final ret = await NimCore.instance.qChatMessageService
            .getMessageHistory(QChatGetMessageHistoryParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateMessage':
        final ret = await NimCore.instance.qChatMessageService
            .updateMessage(QChatUpdateMessageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'revokeMessage':
        final ret = await NimCore.instance.qChatMessageService
            .revokeMessage(QChatRevokeMessageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'deleteMessage':
        final ret = await NimCore.instance.qChatMessageService
            .deleteMessage(QChatDeleteMessageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'markMessageRead':
        final ret = await NimCore.instance.qChatMessageService
            .markMessageRead(QChatMarkMessageReadParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'sendSystemNotification':
        final ret = await NimCore.instance.qChatMessageService
            .sendSystemNotification(
                QChatSendSystemNotificationParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'resendSystemNotification':
        final ret = await NimCore.instance.qChatMessageService
            .resendSystemNotification(
                QChatResendSystemNotificationParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'updateSystemNotification':
        final ret = await NimCore.instance.qChatMessageService
            .updateSystemNotification(
                QChatUpdateSystemNotificationParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'markSystemNotificationsRead':
        final ret = await NimCore.instance.qChatMessageService
            .markSystemNotificationsRead(
                QChatMarkSystemNotificationsReadParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMessageHistoryByIds':
        final ret = await NimCore.instance.qChatMessageService
            .getMessageHistoryByIds(
                QChatGetMessageHistoryByIdsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'replyMessage':
        final ret = await NimCore.instance.qChatMessageService
            .replyMessage(QChatReplyMessageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getReferMessages':
        final ret = await NimCore.instance.qChatMessageService
            .getReferMessages(QChatGetReferMessagesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getThreadMessages':
        final ret = await NimCore.instance.qChatMessageService
            .getThreadMessages(QChatGetThreadMessagesParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMessageThreadInfos':
        final ret = await NimCore.instance.qChatMessageService
            .getMessageThreadInfos(
                QChatGetMessageThreadInfosParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'addQuickComment':
        final ret = await NimCore.instance.qChatMessageService
            .addQuickComment(QChatAddQuickCommentParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'removeQuickComment':
        final ret = await NimCore.instance.qChatMessageService
            .removeQuickComment(QChatRemoveQuickCommentParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getQuickComments':
        final ret = await NimCore.instance.qChatMessageService
            .getQuickComments(QChatGetQuickCommentsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'getMessageCache':
        final ret = await NimCore.instance.qChatMessageService
            .getMessageCache(map['serverId'], map['channelId']);
        handled = false;
        handleCaseResult = ResultBean(
          code: ret.code,
          data: ret.data?.map((e) => e.toJson()).toList(),
          message: ret.errorDetails,
        );
        break;
      case 'clearMessageCache':
        final ret =
            await NimCore.instance.qChatMessageService.clearMessageCache();
        handled = false;
        handleCaseResult = ResultBean(
          code: ret.code,
          data: null,
          message: ret.errorDetails,
        );
        break;
      case 'getLastMessageOfChannels':
        final ret = await NimCore.instance.qChatMessageService
            .getLastMessageOfChannels(
                QChatGetLastMessageOfChannelsParam.fromJson(map));
        result = ret;
        handled = true;
        break;
      case 'searchMsgByPage':
        final ret = await NimCore.instance.qChatMessageService
            .searchMsgByPage(QChatSearchMsgByPageParam.fromJson(map));
        result = ret;
        handled = true;
        break;
    }
    if (handled) {
      print('qChatMessageService#$methodName result: ${result.code}');
      handleCaseResult = ResultBean(
        code: result.code,
        data: result.data?.toJson(),
        message: result.errorDetails,
      );
    }
    if (isCallback) {
      handleCaseResult = ResultBean.success(data: null);
    }
    return handleCaseResult;
  }
}
