// Copyright (c) 2022 NetEase, Inc. All rights reserved.
// Use of this source code is governed by a MIT license that can be
// found in the LICENSE file.

import 'package:hawk/hawk.dart';
import 'package:nim_core/nim_core.dart';

class HandleQChatPushCase extends HandleBaseCase {
  HandleQChatPushCase();

  @override
  Future<ResultBean?> handle(event) async {
    super.handle(event);
    if (className != 'QChatPushService') {
      return null;
    }
    switch (methodName) {
      case 'enableAndroid':
        final ret = await NimCore.instance.qChatPushService
            .enableAndroid(params![0]['enable'] as bool);
        handleCaseResult = ResultBean(
          code: ret.code,
          data: null,
          message: ret.errorDetails,
        );
        break;
      case 'isEnableAndroid':
        final ret = await NimCore.instance.qChatPushService.isEnableAndroid();
        handleCaseResult = ResultBean(
          code: ret.code,
          data: ret.data,
          message: ret.errorDetails,
        );
        break;
      case 'setPushConfig':
        final ret = await NimCore.instance.qChatPushService
            .setPushConfig(QChatPushConfig.fromJson(params![0]));
        handleCaseResult = ResultBean(
          code: ret.code,
          data: null,
          message: ret.errorDetails,
        );
        break;
      case 'getPushConfig':
        final ret = await NimCore.instance.qChatPushService.getPushConfig();
        handleCaseResult = ResultBean(
          code: ret.code,
          data: ret.data?.toJson(),
          message: ret.errorDetails,
        );
        break;
      case 'isPushConfigExistAndroid':
        final ret =
            await NimCore.instance.qChatPushService.isPushConfigExistAndroid();
        handleCaseResult = ResultBean(
          code: ret.code,
          data: ret.data,
          message: ret.errorDetails,
        );
        break;
    }
    return handleCaseResult;
  }
}
